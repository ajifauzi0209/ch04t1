package com.kazakimaru.ch04t1

import android.graphics.Color
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.google.android.material.snackbar.Snackbar
import com.kazakimaru.ch04t1.databinding.FragmentFirstBinding


class FirstFragment : Fragment() {
    private var _binding: FragmentFirstBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentFirstBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        showToast()
        showSnackbar()
        showSnackbarAction()
        showSnackbarDismiss()
        showSnackbarCustom()
    }

    private fun createToast(pesan: String): Toast {
        return Toast.makeText(requireContext(), pesan, Toast.LENGTH_SHORT)
    }

    private fun showToast() {
        binding.btn1Toast.setOnClickListener {
            createToast(getString(R.string.ini_toast)).show()
        }
    }

    private fun showSnackbar() {
        binding.btn2Snackbar.setOnClickListener {
            Snackbar.make(it, "Ini Snackbar", Snackbar.LENGTH_LONG).show()
        }
    }

    private fun showSnackbarAction() {
        binding.btn3Snackbar.setOnClickListener {
            Snackbar.make(it, "Ini Snackbar + Aksi", Snackbar.LENGTH_LONG).setAction("Text Action") {
                createToast("Ini Toast dari Snackbar").show()
            }.show()
        }
    }

    private fun showSnackbarDismiss() {
        binding.btn4Snackbar.setOnClickListener {
            val snackBar = Snackbar.make(it, "Ini snackbar indefinite", Snackbar.LENGTH_INDEFINITE)
            snackBar.setAction(R.string.snackbar_dismiss) {
                snackBar.dismiss()
            }
            snackBar.setBackgroundTint(ContextCompat.getColor(requireContext(), R.color.purple_200))
            snackBar.setActionTextColor(ContextCompat.getColor(requireContext(), R.color.white))
            snackBar.show()
        }
    }

    private fun showSnackbarCustom() {
        binding.btn5Snackbar.setOnClickListener {
            customSnackbar(it,"Ini snackbar custom").show()
        }
    }

    private fun customSnackbar(it: View, pesan: String): Snackbar {
        val senekbar = Snackbar.make(it, pesan, Snackbar.LENGTH_LONG)
        senekbar.setAction("Dismiss") {
            senekbar.dismiss()
        }
        senekbar.setActionTextColor(ContextCompat.getColor(requireContext(), R.color.white))
        senekbar.show()
        return senekbar
    }
}
